# olip-pre-install

This playbook is a preflight step for BSF's OLIP deployments:

* On CMAL100 devices, it prepares the underlying OS
  (mostly, removing unwanted services which conflicts with our ways)
* On all devices, it installs the BSF tooling (Tinc, deploy scripts, etc)

## Warning

This playbook tasks will eventually get merged into [`olip-deploy`](https://gitlab.com/bibliosansfrontieres/olip/olip-deploy).

## Initialization

To be fair, we **never** run this playbook manually.

Should you do so (for development testing purposes for instance),
a convenience script is provided (but not tested):

```shell
curl -sfL https://gitlab.com/bibliosansfrontieres/olip/olip-pre-install/-/raw/master/go.sh | bash -s -- --name idc-fra-name
```
