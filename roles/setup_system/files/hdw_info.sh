#!/bin/bash
FILE="/data/logs/hdw_info.txt"
(
    echo "##################################"
    echo "#          lscpu                 #"
    echo "##################################"
    lscpu
    echo "##################################"
    echo "#          lshw                  #"
    echo "##################################"
    # dont run as root - bibliosansfrontieres/olip/olip-pre-install#39
    sudo -u nobody lshw -short
    echo "##################################"
    echo "#          hwinfo                #"
    echo "##################################"
    hwinfo --short
    echo "##################################"
    echo "#          lscpi                 #"
    echo "##################################"
    lspci
    echo "##################################"
    echo "#          lsusb                 #"
    echo "##################################"
    lsusb
    echo "##################################"
    echo "#          lsblk                 #"
    echo "##################################"
    lsblk
    echo "##################################"
    echo "#          Disk space            #"
    echo "##################################"
    df
    echo "##################################"
    echo "#           IP address           #"
    echo "##################################"
    ip a l
    echo "##################################"
    echo "#          Process               #"
    echo "##################################"
    ps aufx
    echo "##################################"
    echo "#          systemctl             #"
    echo "##################################"
    systemctl status
    echo "##################################"
    echo "#          Public IP address     #"
    echo "##################################"
    wget -qO- http://ipecho.net/plain
    echo "##################################"
    echo "#          Balena ps             #"
    echo "##################################"
    balena-engine ps -a
    echo "##################################"
    echo "#          Balena images         #"
    echo "##################################"
    balena-engine images
    echo
) > $FILE
